# Get Started

**Usage of the Terraform Script**

1. **Clone this repo**
2. **cd terraform-modules**
3. **Install Terraform**
4. **Run `terraform init`**
5. **Run `terraform plan -var "aws_profile=profile_name" -var "aws_region=region_name" -var "key=Public-Key"`