variable "cidr" {
  description = "CIDR Block for the VPC"
  default     = "172.16.0.0/27"
}

variable "cidr_private_subnet" {
  description = "Private Subnet CIDR"
  default     = "172.16.0.0/28"
}

variable "cidr_public_subnet" {
  description = "Public Subnet CIDR"
  default     = "172.16.0.16/28"
}

variable "env_name" {
  description = "Name of the Environment"
  default     = "UAT"
}

variable "anywhere" {
  description = "Anywhere CIDR Notation"
  default     = "0.0.0.0/0"
}

# Variables from the other Modules
variable "aws_region" {}
