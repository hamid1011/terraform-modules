output "aws_vpc_default_id" {
  value = "${aws_vpc.default.id}"
}

output "anywhere" {
  value = "${var.anywhere}"
}

output "env_name" {
  value = "${var.env_name}"
}

output "aws_public_subnet_id" {
  value = "${aws_subnet.public_az_1.id}"
}

output "aws_private_subnet_id" {
  value = "${aws_subnet.private_az_1.id}"
}
