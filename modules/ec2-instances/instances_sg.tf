resource "aws_security_group" "ec2_instances" {
  name        = "${var.sg_name}"
  vpc_id      = "${var.aws_vpc_default_id}"
  description = "Security group for EC2 instances"

  # SSH access from within the VPC only (to access EC2 instances create a temporary bastion host)
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.anywhere}"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.anywhere}"]
  }

  # Outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.anywhere}"]
  }

  tags {
    Environment = "${var.env_name}"
  }
}
