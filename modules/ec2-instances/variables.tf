# Variables from This Modules
variable "sg_name" {
  description = "Name of the Security Group"
  default     = "ec2_instances_sg"
}

variable "key" {
  description = "Key To be used to login to the server"
  default     = ""
}

# Variables from Other Modules

variable "aws_vpc_default_id" {}
variable "anywhere" {}
variable "env_name" {}
variable "aws_public_subnet_id" {}
variable "aws_private_subnet_id" {}
