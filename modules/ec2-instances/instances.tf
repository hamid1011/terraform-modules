resource "aws_key_pair" "ec2_instances" {
  key_name   = "ec2_key"
  public_key = "${var.key}"
}

resource "aws_instance" "web_server" {
  ami                         = "ami-81cefcfd"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.ec2_instances.id}"
  vpc_security_group_ids      = ["${aws_security_group.ec2_instances.id}"]
  subnet_id                   = "${var.aws_public_subnet_id}"
  associate_public_ip_address = true

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 20
    delete_on_termination = true
  }

  tags {
    Name      = "NginX"
    ManagedBy = "Terraform"
    Role      = "web"
  }
}

resource "aws_instance" "application_server" {
  ami                         = "ami-81cefcfd"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.ec2_instances.id}"
  vpc_security_group_ids      = ["${aws_security_group.ec2_instances.id}"]
  subnet_id                   = "${var.aws_private_subnet_id}"
  associate_public_ip_address = false

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 20
    delete_on_termination = true
  }

  tags {
    Name      = "Tomcat"
    ManagedBy = "Terraform"
    Role      = "application"
  }
}
