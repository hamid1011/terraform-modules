# Variables for the Env Specific

variable "aws_profile" {}
variable "aws_region" {}

# Variabled for the Other Modules Passed here

variable "env_name" {}
variable "cidr" {}
variable "cidr_private_subnet" {}
variable "cidr_public_subnet" {}
variable "key" {}
