# Provider Details
provider "aws" {
  profile = "${var.aws_profile}"
  region  = "${var.aws_region}"
}

module "networking" {
  source     = "../modules/networking"
  aws_region = "${var.aws_region}"
}

module "ec2-instances" {
  source                = "../modules/ec2-instances"
  aws_vpc_default_id    = "${module.networking.aws_vpc_default_id}"
  anywhere              = "${module.networking.anywhere}"
  env_name              = "${module.networking.env_name}"
  aws_public_subnet_id  = "${module.networking.aws_public_subnet_id}"
  aws_private_subnet_id = "${module.networking.aws_private_subnet_id}"
  key                   = "${var.key}"
}
